/*
Naive Map/Reduce Method
=======================
This method is quick to code but very memory inefficient, requiring the program to 
first generate an Array containing MAX_VAL objects. Only then can it begin to compute
the sum of multiples.
*/
if (process.argv.length != 3){
	console.log("./run.js 1000")
	console.log('args: ', process.argv)
}

const max_value = parseInt(process.argv[2])

function sum_multiples(max_val){
	return [...Array(max_val).keys()].filter(x => x % 3 == 0 || x % 5 == 0).reduce( (x,y) => x + y)
}

console.log("Sum:", sum_multiples(max_value))
