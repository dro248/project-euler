/*
Functional Accumulator Method
=============================
This method is doesn't require the program to generate a huge array to get started by implementing
a "functional" (as in "functional programming") accumulator approach.
This approach should theoretically work provided that the runtime implements TCO. This, however
is not the case with the current version of Node (v16), and thus runs into recursion depth problems.
*/
if (process.argv.length != 3){
	console.log("./run.js 1000")
	console.log('args: ', process.argv)
}

// Max value (as given by the user)
const max_value = parseInt(process.argv[2])


function euler_sum(current_val, max_val, accumulator){
	/*
		current_val: the current value we're looking at; range: [0, max_val)
		max_val: the upper limit; when to stop; I wove this in to make this more "functional"
		accumulator: the running sum of values
	*/

	// base case
	if (current_val >= max_val){
		return accumulator
	}

	// Recurse: make sure to use "return" in order to form the return chain
	return euler_sum(
		current_val+1, 
		max_val,
		(current_val % 3 == 0 || current_val % 5 == 0) ? accumulator + current_val : accumulator 
	)
}

console.log("Sum:", euler_sum(0, max_value, 0))
