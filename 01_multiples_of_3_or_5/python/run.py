def sum_of_multiples(max_val: int) -> int:
    return sum(n for n in range(max_val) if n % 3 == 0 or n % 5 == 0)


if __name__ == "__main__":
    import sys

    # print usage
    if len(sys.argv) != 2:
        print("./run.py 1000")

    max_value = int(sys.argv[1])

    print(f"Sum: {sum_of_multiples(max_value)}")
