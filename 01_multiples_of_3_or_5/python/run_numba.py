"""
# RunNumba

Numba is FAST (like VERY fast!).
The problems we run into are:
- code is isn't very Pythonic or succinct (so that Numba jit compiler can figure it out)
- Numba converts Python Integers to int64 (which get maxed out at `max_val=1B`)
"""
import sys
from numba import jit

@jit(nopython=True)
def eulerSum(x): 
	# return sum(n for n in range(0, x, 3)) + sum(n for n in range(0, x, 5)) - sum(n for n in range(0, x, 15))
	sum_3 = 0
	sum_5 = 0
	sum_15 = 0
	for i in range(0, x, 3):
		sum_3 += i

	for i in range(0, x, 5):
		sum_5 += i

	for i in range(0, x, 15):
		sum_15 += i

	return sum_3 + sum_5 - sum_15


if __name__ == '__main__':
	if len(sys.argv) < 2:
		print("python run.py MAX_VAL")
		sys.exit(-1)	

	max_val = int(sys.argv[1])

	print(f"Sum: {eulerSum(max_val)}")
