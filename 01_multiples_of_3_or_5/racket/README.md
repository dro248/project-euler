# Sum of Multiples (3 | 5)
The Racket implementation has the "range 1000" baked in.

# Run from interpreter
racket run.rkt

# Compile to executable
raco exe run.rkt  # returns `run` executable

# Run executable
./run
