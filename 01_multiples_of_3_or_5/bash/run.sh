#!/bin/bash

sum=0;
for i in {1..999}; do
    if [[ $(expr $i % 3) = "0" || $(expr $i % 5) = "0" ]]; then
		sum=$((sum+$i));
    fi
done

echo $sum
