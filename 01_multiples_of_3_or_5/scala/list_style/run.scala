
object Run {
	val eulerSum = (x:Long) => List.range(1, x).view.filter({ n => n % 3 == 0 || n % 5 == 0 }).sum

	def main(args: Array[String]) = {
		val max_val = args(0).toLong

		val my_sum = eulerSum(max_val)
		println(s"Sum: $my_sum")
	}
}
