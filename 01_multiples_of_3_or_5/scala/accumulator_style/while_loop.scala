/*
While Loop Style
This implementation allows us to break the MAX_VAL = 1B mark. (Cool)
*/

object WhileLoopStyle {

	def eulerSum(max_val: BigInt): BigInt = {
		var acc: BigInt = 0

		var i: BigInt = 1
		// JVM Problem with for loop: Even though we are using BigInts, the Range obj we create
		// is limited to Int.MaxValue (i.e. use a while loop)
		//for(i <- BigInt(1) to (max_val-1)) {

		while (i < max_val) {
			if (i % 3 == 0 || i % 5 == 0) {
				acc += i
			}
			i+=1
		}
		acc
	}

	def main(args: Array[String]) = {
		// parse `max_val` from command-line
		val max_val = BigInt(args(0))

		val my_sum = eulerSum(max_val)
		println(s"Sum: $my_sum")
	}
}
