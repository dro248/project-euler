/*
For Loops Style:
This style is limited by:
- the accumulator (`acc`) must fit within a LONG
- the Range cannot be larger than Int.MaxValue in size

For very large computations, see implementation while_loop.scala
*/

object ForLoopStyle {

	def eulerSum(max_val: Long): Long = {
		var acc: Long = 0

		for(i <- 1L to (max_val-1)) {
			if (i % 3 == 0 || i % 5 == 0) {
				acc += i
			}
		}
		acc
	}

	def main(args: Array[String]) = {
		// parse `max_val` from command-line
		val max_val = args(0).toLong

		val my_sum = eulerSum(max_val)
		println(s"Sum: $my_sum")
	}
}
