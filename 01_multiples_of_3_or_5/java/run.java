
class SumOfMultiples {
	public static void main(String[] args) {
		if (args.length < 1){
			System.out.println("java run.java MAX_VAL");
			
		}

		int MAX_VAL = Integer.parseInt(args[0]);

		long sum = 0;
		for (int i = 0; i < MAX_VAL; i++){
			if (i % 3 == 0 || i % 5 == 0){
				sum += i;
			}
		}

		System.out.println(sum);
	}
}
