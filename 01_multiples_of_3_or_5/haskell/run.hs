import System.Environment

strToInt s = read s::Int
eulerSum x = sum [n | n <- [1,2..(x-1)], (n `mod` 3==0 || n `mod` 5==0)]

main = do
    args <- getArgs
    putStrLn ("Sum: " ++ show (eulerSum (strToInt (args!!0))))
