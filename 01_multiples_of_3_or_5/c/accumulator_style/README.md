# While / For Accumulator styles

These take an accumulator approach (one with a `for` loop, the other with a `while`).

Tips:
- max MAX_VAL: 1B (1,000,000,000) before overflow of unsigned long long sum
- compile with: gcc -O2 or -O3 for max C speed (this makes a huge (~10x) difference!)
