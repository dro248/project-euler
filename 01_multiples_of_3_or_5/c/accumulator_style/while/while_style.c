#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int main(int argc, char* argv[]){
	unsigned long long max_value = atoi(argv[1]);
	
	unsigned long long i = 0;
	unsigned long long sum = 0;
	while (i < max_value){
		if (i % 3 == 0 || i % 5 == 0){
			sum += i;
		}
		++i;
	}

	printf("Sum: %llu\n", sum);
	return 0;
}
