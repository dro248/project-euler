#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int main(int argc, char* argv[]){
	unsigned long long max_value = atoi(argv[1]);
	// printf("Max Value: %i\n", max_value);
	
	unsigned long long sum = 0;
	for (unsigned long long i = 0; i < max_value; ++i){
		if (i % 3 == 0 || i % 5 == 0){
			sum += i;
		}
	}

	printf("Sum: %llu\n", sum);
	return 0;
}
