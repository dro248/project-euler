#include <stdio.h>
#include <stdlib.h>
#include <string.h>

unsigned long long range_sum(unsigned long long start, unsigned long long stop, unsigned long long step){
	unsigned long long sum = 0;
	for (unsigned long long i = start; i < stop; i+=step){
		sum += i;
	}
	return sum;
}

int main(int argc, char* argv[]){
	unsigned long long max_value = atoi(argv[1]);
	
	// Aggregate the sum of ranges
	unsigned long long sum = range_sum(0, max_value, 3) + range_sum(0, max_value, 5) - range_sum(0, max_value, 15);

	printf("Sum: %llu\n", sum);
	return 0;
}
