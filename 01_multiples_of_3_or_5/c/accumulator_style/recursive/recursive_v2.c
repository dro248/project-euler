#include <stdio.h>
#include <stdlib.h>
#include <string.h>


unsigned long long euler_sum(unsigned long long current_val, unsigned long long max_val, int step_size, unsigned long long accumulator){
	if (current_val >= max_val)
		return accumulator;

	return euler_sum(
		current_val + step_size,
		max_val,
		step_size,
		accumulator + current_val
	);
}

int main(int argc, char* argv[]){
	unsigned long long max_value = atoi(argv[1]);
	
	unsigned long long total_sum = euler_sum(0, max_value, 3, 0) + euler_sum(0, max_value, 5, 0) - euler_sum(0, max_value, 15, 0);
	printf("Sum: %llu\n", total_sum);
	return 0;
}
