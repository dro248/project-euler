#include <stdio.h>
#include <stdlib.h>
#include <string.h>


unsigned long long euler_sum(unsigned long long current_val, unsigned long long max_val, unsigned long long accumulator){
	if (current_val >= max_val)
		return accumulator;

	return euler_sum(
		current_val + 1,
		max_val,
		(current_val % 3 == 0 || current_val % 5 == 0) ? accumulator + current_val : accumulator
	);
}

int main(int argc, char* argv[]){
	unsigned long long max_value = atoi(argv[1]);

	printf("Sum: %llu\n", euler_sum(0, max_value, 0));
	return 0;
}
