# Malloc Array Style

This works by first generating a list of size MAX_VAL (based on user input)
and then adding multiples of 3 or 5. Not efficient (at all) but it's a nice example of malloc. 

Tip: max value for MAX_VAL = 100,000,000
