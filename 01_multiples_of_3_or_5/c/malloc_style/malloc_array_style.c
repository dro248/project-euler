#include <stdio.h>
#include <stdlib.h>
#include <string.h>

long sum(long* multiples, int m_length){
	long sum = 0;

	for (long i = 0; i < m_length; i++){
		sum += multiples[i];
	} 

	return sum;
}


long sum_of_multiples(long max_val){
	long* arr = (long*) malloc (max_val * sizeof(long));

	// Adds the values that are multiples of 3 or 5 to the array (simple but not efficient)
	for (long i = 0; i < max_val; i++){
		if (i % 3 == 0 || i % 5 == 0){
			arr[i] = i;
		}
	}

	long total = sum(arr, max_val);
	free(arr);
	return total;
}


int main(int argc, char* argv[]){
	long max_value = atoi(argv[1]);
	// printf("Max Value: %i\n", max_value);

	printf("Sum: %ld\n", sum_of_multiples(max_value));
	return 0;
}
